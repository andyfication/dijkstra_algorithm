//Main Javascript function. Handles the program 
$(document).ready(() => {
  let init = setInterval(() => {
    init_app(init);
  }, 100);
});

//This will be used for the coursera API
courseraApi.callMethod({
  type: "GET_SESSION_CONFIGURATION",
  onSuccess: init_coursera
});

// Called by coursera API
function init_coursera() {
  let init = setInterval(() => {
    init_app(init);
  }, 100);
}

// Initialise the App
function init_app(init) {
  // if on main container, load the app
  if (mainAppStarted) {
    // Initialise tutorial
    let tutorial = new Tutorial();
    tutorial.setTutorialLayout();
    tutorial.initTutorial();
    // Initialise network manager and network construction
    let networkManager = new NetworkManager();
    // Initialise variable for the dij validator
    let dijkstraValidator = null;
    // Initialise variable for the table validator
    let tableValidator = null;
    // Solution variable 
    let solution = null;
    // Only make the graph interactive once positioned 
    let initGame = setInterval(() => {
      if (networkManager.graphPositioned) {
        // Clean the network selection (reset colours after interaction)
        networkManager.network.unselectAll();
        // Lock the drag option for each node of the graph
        var options = {
          interaction: {
            dragNodes: false,
            dragView: true
          }
        }
        // Set the initial graph options
        networkManager.network.setOptions(options);
        // Only compute solution and calculation when the route is set 
        if (networkManager.routeIsSet) {
          // Initialise the algorithm validator
          dijkstraValidator = new DijkstraValidator(networkManager.network);
          // Construct raw version copy of the vis library graph
          dijkstraValidator.constructGraph();
          // Test the validator with start and end node
          solution = dijkstraValidator.calculateShortestPath(networkManager.startNode, networkManager.endNode);
          // Assign nodes to solution list
          dijkstraValidator.assignNodesToList();
          // Set the table manager here
          tableValidator = new tableManager(dijkstraValidator.map, networkManager, solution);
          tutorial.tutorialTasks[tutorial.currentTutorialCount].text = tableValidator.initTable();
          tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
          // Clear the interval 
          clearInterval(initGame);
          // The graph is no longer positioned
          networkManager.graphPositioned = false;
          // The route is no longer set
          networkManager.routeIsSet = false;
        }
      }
    });

    //clear the interval for initApp. Only call it once after the intro animation
    clearInterval(init);

    // Event Handlers-------------------------------------------------------

    // Waiting for the graph to be created before setting the pointer on hover for the nodes
    let waitForGraphCreation = setInterval(() => {
      if (networkManager.graphCreated) {
        // Event Handlers for node pointer 
        networkManager.network.on("hoverNode", function (params) {
          networkManager.network.canvas.body.container.style.cursor = 'pointer';
        });
        networkManager.network.on("blurNode", function (params) {
          networkManager.network.canvas.body.container.style.cursor = 'default';
        });

        // Check which nodes we are clicking
        networkManager.network.on('click', function (properties) {
          let ids = properties.nodes;
          let clickedNode = networkManager.nodes.get(ids);

          // Only if clicking on a node
          if (clickedNode.length != 0) {
            let clickedNodeId = clickedNode[0].id;
            // Check start node click 
            if (tutorial.currentTutorialCount == 11) {
              // Assign node to the network manager 
              networkManager.setStartNode(clickedNodeId);
              networkManager.startNode = networkManager.cleanNodeLable((networkManager.nodes.get(clickedNodeId).label));
              networkManager.currentId = clickedNodeId;
              // Move to next task
              tutorial.currentTutorialCount += 1;
              tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
              // Check end node click
            } else if (tutorial.currentTutorialCount == 12) {
              // Only if start and end nodes are different set the end node and route 
              if (networkManager.endNodeDiffersFromStart(clickedNodeId)) {
                networkManager.endNode = networkManager.cleanNodeLable((networkManager.nodes.get(clickedNodeId).label));
                networkManager.endNodeId = clickedNodeId;
                // Move to next task 
                tutorial.currentTutorialCount += 1;
                networkManager.routeIsSet = true;
                // No end is set if equal to start 
              } else {
                networkManager.endNode = null;
                tutorial.tutorialTasks[tutorial.currentTutorialCount].text = 'Your journey starts from this station, please select another destination';
              }
              tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
              // Check node click on table construction
            } else if (tutorial.currentTutorialCount == 13) {
              // Check if clicked node is valid for the algorithm steps
              tutorial.tutorialTasks[tutorial.currentTutorialCount].text = tableValidator.dynamicTableConstruction(clickedNodeId);;
              tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
            }
          }
        });
        // No longer need to run this scope
        clearInterval(waitForGraphCreation);
      }
    });

    // Increment tutorial task on button click 
    $('#suggestion').on('click', '.button-next', () => {
      // Position the graph tutorial section 
      if (tutorial.currentTutorialCount == 10) {
        networkManager.graphPositioned = true;
        tutorial.currentTutorialCount += 1;
        tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
        // Answer section 
      } else if (tutorial.currentTutorialCount == 13) {
        tutorial.currentTutorialCount += 1;
        tutorial.tutorialTasks[tutorial.currentTutorialCount].text = 'What is the shortest journey from \'' + networkManager.startNode + '\' to \'' + networkManager.endNode + '\' ?';
        tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
        // Any other section of the tutorial
      } else {
        tutorial.currentTutorialCount += 1;
        tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
      }
    });

    //Go back from answer section using the back button
    $('#solution-back-button').on('click', () => {
      tutorial.currentTutorialCount = 13;
      tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
    });

    // Add answer options to the solution list
    $('#node-label-list li').on('click', function () {
      // Push the item in the solution array
      tableValidator.userSolution.push($(this).val());
      // Create a li - span element and add it to the solution list
      let listReference = $('#solution-label-list');
      let liNode = document.createElement('li');
      let spanNode = document.createElement('span');
      spanNode.classList.add("solution-item-text");
      let spanTextNode = document.createTextNode($(this).find('span').text());
      spanNode.appendChild(spanTextNode);
      liNode.appendChild(spanNode);
      liNode.classList.add('solution-item');
      $(listReference).append(liNode);
    });

    // Empty the solution list
    $('#solution-clear-button').on('click', function () {
      $('#solution-label-list').empty();
      tableValidator.userSolution.length = 0;
    });

    // Check the final submission
    $('#solution-submit-button').on('click', function () {
      //Add highlight to buttons
      $('#interactive-table-buttons-container').addClass('highlightBorder');
      // If correct answer is given 
      if (tableValidator.areArraysEqual(tableValidator.userSolution, tableValidator.solution['path'])) {
        tutorial.tutorialTasks[tutorial.currentTutorialCount].text = 'You have succesfully found the shortest journey from \'' + networkManager.startNode + '\' to \'' + networkManager.endNode + '\'';
        tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
        // If wrong answer is given 
      } else {
        tutorial.tutorialTasks[tutorial.currentTutorialCount].text = 'Unfortunately this is not the shortest journey from \'' + networkManager.startNode + '\' to \'' + networkManager.endNode + '\'';
        tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
      }
      // Remove solution container, only one attempt. The exercise can be repeted
      $('#solution-container').css('display', 'none');
      // Reset lists for the solution
      $('#solution-label-list').empty();
      tableValidator.userSolution.length = 0;
    });

    //Jump the tutorial section 
    $('#suggestion').on('click', '.button-skip', () => {
      tutorial.currentTutorialCount = 9;
      tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
    });

    // Toggle the Graph Legenda on help button click 
    $('#help').on('click', () => {
      $(".modal-title").html('Dijkstra’s Algorithm');
      $(".modal-body").html('<h3 style="margin-top:30px">Graph</h3><p><strong>Position the Graph:</strong> Click one of the edges and drag</p> <p><strong>Zoom in and Out:</strong> Use the mouse wheel</p> <p><strong>Select Nodes: </strong> Click the desidered node</p> <p style="margin-bottom:40px"><strong>Drag Nodes:</strong> Click and drag the desidered node</p> <h3>Nodes</h3> <p><strong>Nodes</strong> represent each station of the current underground network.</p> <p>Each <strong>node</strong> has a label displaying the name of the station and the distance from the starting station:</p><p> <img src = "images/help_node.png" id ="help-node"/></p> <p><strong>Name of the station:</strong> Piccadilly Circus [PC]</p><p><strong>Distance from the starting station:</strong> 0.5Km</p> <h3 style="margin-top:30px">Edges</h3><p><strong>Edges</strong> represent each route between two stations of the current underground network.</p><p>Each <strong>edge</strong> has a label displaying the distance in Km between two stations: </p> <p> <img src = "images/help_edge.png" id ="help-edge"/></p><p>The distance between <strong>Piccadilly Circus</strong>  and <br>  <strong>Oxford Circus</strong> is 1km.</p> <h3 style="margin-top:30px">Table</h3><p>The <strong>Dijkstra’s table</strong> will help you to visualise the distances between your starting station and all the other stations of the current underground network.</p><p> <img src = "images/help_table.png" id ="help-table"/></p><p>The first <strong>table</strong> row shows all the stations of the current underground network.</p><p>The first <strong>table</strong> column shows the station that you are <br> currently analysing.</p><p>Each <strong>table</strong> cell shows the distance between two stations, recoding also the station that you are coming from:</p><p> <img src = "images/help_cell.png" id ="help-cell"/></p><p>Green <strong>table</strong> cells represent a finalised station; there is not a shortest route from this station:</p><p> <img src = "images/help_green_cell.png" id ="help-green-cell"/></p><p><strong>Table</strong> cells with the infinity symbol show that there is not a <br> direct route between the two stations:</p><p> <img src = "images/help_infinity.png" id ="help-infinity"/></p><h3 style="margin-top:30px">Overview</h3><p>This simulation helps you to visualise how <strong>Dijkstra’s Algorithm</strong> computes the shortest path between two nodes. Each node of the graph correspond to a specific underground station. Nodes are connected with weighted edges. The label on each edge carries information about the distance in Km between two stations. <strong>Dijkstra’s Algorithm</strong> can then be used to determine the shortest journey between two stations.</p><h3 style="margin-top:30px">How to play</h3><p>Once you have successfully positioned the underground graph so that it is readable and clear, you have to select where you want to begin and end your journey. You can do so by selecting two stations on the graph.</p><p>At this point, the <strong>Dijkstra’s algorithm</strong> will calculate all the distances between the current station and all the direct neighbours of the current station. You need to select each directly connected station to the current station on the graph to update and record the distances. The distances will be updated on each node and recorded on the table.</p><p>Once you have successfully visited all the neighbours of the current station, the current table row will start flashing and will highlight all the possible stations to where to move next.</p><p><strong> Dijkstra’s algorithm</strong> will choose the shortest distance as the next station to visit.</p> <p>Look at the table, find the shortest distance among the highlighted cells and select the corresponding station on the graph.</p><p> <img src = "images/help_table_flash.png" id ="help-table-flash"/></p><p>In this particular scenario for example, the shortest distance is 0.6Km among the highlighted options.</p><p>The closest station to <strong>Oxford Street</strong> is therefore <strong>Bond Street</strong>.</p><p>At this point you need to repeat the same process for all the remaining stations of the underground network.</p><h3 style="margin-top:30px">Submit the solution</h3><p>Once you have visited and finalised all the stations, the table will result complete and will show all the distances between the starting station and all the others. <br> The graph will highlight the shortest path to your journey:</p><p> <img src = "images/help_graph.png" id ="help-graph"/></p><p> Furthermore the table will highlight the shortest path to your journey from top to bottom of the first column:</p><p> <img src = "images/help_solution.png" id ="help-solution"/></p><p>At this point you can submit your answer by either looking at the highlighted path on the graph or by looking at the table.</p><p> In this example the answer is : Oxford Street, Piccadilly Circus, Leicester Square.');
      $(".modal").modal("show");
    });

    // Toggle the Graph Underground Name on/off
    $('#network-name-toggle-container').on('click', function () {
      $(this).toggleClass('change');
      $('.network-name-container').toggleClass('network-name-container-hide');
    });

    // Underground map selection-----------------------------

    // London--------------
    $('#london-icon, #london-popup').on('click', function () {
      $('#network-name').text('LONDON');
      $('#map-container').fadeOut(1000);
      // Initialise graph
      networkManager.networkId = 'london';
      networkManager.initialSetup();
      networkManager.assignNodesandEdgesToNetwork();
      networkManager.initNetwork();
      tutorial.currentTutorialCount = 2;
      tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
      tutorial.currentGraphSelection = 'london';
      networkManager.graphCreated = true;
      $('.icon-active').css('pointer-events', 'none');

    });
    // Paris--------------
    $('#paris-icon, #paris-popup').on('click', function () {
      $('#network-name').text('PARIS');
      $('#map-container').fadeOut(1000);
      // Initialise graph
      networkManager.networkId = 'paris';
      networkManager.initialSetup();
      networkManager.assignNodesandEdgesToNetwork();
      networkManager.initNetwork();
      tutorial.currentTutorialCount = 2;
      tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
      tutorial.currentGraphSelection = 'paris';
      networkManager.graphCreated = true;
      $('.icon-active').css('pointer-events', 'none');
    });
    // Tokyo--------------
    $('#tokyo-icon, #tokyo-popup').on('click', function () {
      $('#network-name').text('TOKYO');
      $('#map-container').fadeOut(1000);
      // Initialise graph
      networkManager.networkId = 'tokyo';
      networkManager.initialSetup();
      networkManager.assignNodesandEdgesToNetwork();
      networkManager.initNetwork();
      tutorial.currentTutorialCount = 2;
      tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
      tutorial.currentGraphSelection = 'tokyo';
      networkManager.graphCreated = true;
      $('.icon-active').css('pointer-events', 'none');
    });
    // Delhi--------------
    $('#delhi-icon, #delhi-popup').on('click', function () {
      $('#network-name').text('DELHI');
      $('#map-container').fadeOut(1000);
      // Initialise graph
      networkManager.networkId = 'delhi';
      networkManager.initialSetup();
      networkManager.assignNodesandEdgesToNetwork();
      networkManager.initNetwork();
      tutorial.currentTutorialCount = 2;
      tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
      tutorial.currentGraphSelection = 'delhi';
      networkManager.graphCreated = true;
      $('.icon-active').css('pointer-events', 'none');
    });
    // Washington--------------
    $('#washington-icon, #washington-popup').on('click', function () {
      $('#network-name').text('WASHINGTON');
      $('#map-container').fadeOut(1000);
      // Initialise graph
      networkManager.networkId = 'washington';
      networkManager.initialSetup();
      networkManager.assignNodesandEdgesToNetwork();
      networkManager.initNetwork();
      tutorial.currentTutorialCount = 2;
      tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
      tutorial.currentGraphSelection = 'washington';
      networkManager.graphCreated = true;
      $('.icon-active').css('pointer-events', 'none');
    });

    // Reset current network
    $('#reset-button').on('click', function () {
      //Remove highlight from buttons
      $('#interactive-table-buttons-container').removeClass('highlightBorder');
      // Set the right task
      tutorial.currentTutorialCount = 9;
      tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
      // Initialise network manager and network construction
      networkManager = new NetworkManager();
      // Initialise graph
      networkManager.networkId = tutorial.currentGraphSelection;
      networkManager.initialSetup();
      networkManager.assignNodesandEdgesToNetwork();
      networkManager.initNetwork();
      // Done creating the graph
      networkManager.graphCreated = true;
      // Initialise variable for the dij validator
      dijkstraValidator = null;
      // Solution variable 
      solution = null;
      // Reset all the table cells
      tableValidator.resetAllTableCells();
      // Only make the graph interactive once positioned 
      let initGame = setInterval(() => {
        if (networkManager.graphPositioned) {
          // Clean the network selection (reset colours after interaction)
          networkManager.network.unselectAll();
          // Lock the drag option for each node of the graph
          var options = {
            interaction: {
              dragNodes: false,
              dragView: true
            }
          }
          // Set the initial graph options
          networkManager.network.setOptions(options);
          // Only compute solution and calculation when the route is set 
          if (networkManager.routeIsSet) {
            // Initialise the algorithm validator
            dijkstraValidator = new DijkstraValidator(networkManager.network);
            // Construct raw version copy of the vis library graph
            dijkstraValidator.constructGraph();
            // Test the validator with start and end node
            solution = dijkstraValidator.calculateShortestPath(networkManager.startNode, networkManager.endNode);
            // Assign nodes to solution list
            dijkstraValidator.assignNodesToList();
            // Set the table manager here
            tableValidator = new tableManager(dijkstraValidator.map, networkManager, solution);
            tutorial.tutorialTasks[tutorial.currentTutorialCount].text = tableValidator.initTable();
            tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
            // Clear the interval 
            clearInterval(initGame);
            // The graph is no longer positioned
            networkManager.graphPositioned = false;
            // The route is no longer set
            networkManager.routeIsSet = false;
          }
        }
      });
      // Waiting for the graph to be created before setting the pointer on hover for the nodes
      let waitForGraphCreation = setInterval(() => {
        if (networkManager.graphCreated) {
          // Event Handlers for node pointer 
          networkManager.network.on("hoverNode", function (params) {
            networkManager.network.canvas.body.container.style.cursor = 'pointer';
          });
          networkManager.network.on("blurNode", function (params) {
            networkManager.network.canvas.body.container.style.cursor = 'default';
          });

          // Check which nodes we are clicking
          networkManager.network.on('click', function (properties) {
            let ids = properties.nodes;
            let clickedNode = networkManager.nodes.get(ids);

            // Only if clicking on a node
            if (clickedNode.length != 0) {
              let clickedNodeId = clickedNode[0].id;
              // Check start node click 
              if (tutorial.currentTutorialCount == 11) {
                // Assign node to the network manager 
                networkManager.setStartNode(clickedNodeId);
                networkManager.startNode = networkManager.cleanNodeLable((networkManager.nodes.get(clickedNodeId).label));
                networkManager.currentId = clickedNodeId;
                // Move to next task
                tutorial.currentTutorialCount += 1;
                tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
                // Check end node click
              } else if (tutorial.currentTutorialCount == 12) {
                // Only if start and end nodes are different set the end node and route 
                if (networkManager.endNodeDiffersFromStart(clickedNodeId)) {
                  networkManager.endNode = networkManager.cleanNodeLable((networkManager.nodes.get(clickedNodeId).label));
                  networkManager.endNodeId = clickedNodeId;
                  // Move to next task 
                  tutorial.currentTutorialCount += 1;
                  networkManager.routeIsSet = true;
                  // No end is set if equal to start 
                } else {
                  networkManager.endNode = null;
                  tutorial.tutorialTasks[tutorial.currentTutorialCount].text = 'Your journey starts from this station, please select another destination';
                }
                tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
                // Check node click on table construction
              } else if (tutorial.currentTutorialCount == 13) {
                // Check if clicked node is valid for the algorithm steps
                tutorial.tutorialTasks[tutorial.currentTutorialCount].text = tableValidator.dynamicTableConstruction(clickedNodeId);
                tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
              }
            }
          });
          // No longer need to run this scope
          clearInterval(waitForGraphCreation);
        }
      });
    });


    // New network from map selection 
    $('#random-button').on('click', function () {
      //Remove highlight from buttons
      $('#interactive-table-buttons-container').removeClass('highlightBorder');
      // Set the right task
      tutorial.currentTutorialCount = 1;
      tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
      // Initialise network manager and network construction
      networkManager = new NetworkManager();
      // Initialise variable for the dij validator
      dijkstraValidator = null;
      // Solution variable 
      solution = null;
      // Reset all the table cells
      tableValidator.resetAllTableCells();
      // Only make the graph interactive once positioned 
      let initGame = setInterval(() => {
        if (networkManager.graphPositioned) {
          // Clean the network selection (reset colours after interaction)
          networkManager.network.unselectAll();
          // Lock the drag option for each node of the graph
          var options = {
            interaction: {
              dragNodes: false,
              dragView: true
            }
          }
          // Set the initial graph options
          networkManager.network.setOptions(options);
          // Only compute solution and calculation when the route is set 
          if (networkManager.routeIsSet) {
            // Initialise the algorithm validator
            dijkstraValidator = new DijkstraValidator(networkManager.network);
            // Construct raw version copy of the vis library graph
            dijkstraValidator.constructGraph();
            // Test the validator with start and end node
            solution = dijkstraValidator.calculateShortestPath(networkManager.startNode, networkManager.endNode);
            // Assign nodes to solution list
            dijkstraValidator.assignNodesToList();
            // Set the table manager here
            tableValidator = new tableManager(dijkstraValidator.map, networkManager, solution);
            tutorial.tutorialTasks[tutorial.currentTutorialCount].text = tableValidator.initTable();
            tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
            // Clear the interval 
            clearInterval(initGame);
            // The graph is no longer positioned
            networkManager.graphPositioned = false;
            // The route is no longer set
            networkManager.routeIsSet = false;
          }
        }
      });
      // Waiting for the graph to be created before setting the pointer on hover for the nodes
      let waitForGraphCreation = setInterval(() => {
        if (networkManager.graphCreated) {
          // Event Handlers for node pointer 
          networkManager.network.on("hoverNode", function (params) {
            networkManager.network.canvas.body.container.style.cursor = 'pointer';
          });
          networkManager.network.on("blurNode", function (params) {
            networkManager.network.canvas.body.container.style.cursor = 'default';
          });

          // Check which nodes we are clicking
          networkManager.network.on('click', function (properties) {
            let ids = properties.nodes;
            let clickedNode = networkManager.nodes.get(ids);

            // Only if clicking on a node
            if (clickedNode.length != 0) {
              let clickedNodeId = clickedNode[0].id;
              // Check start node click 
              if (tutorial.currentTutorialCount == 11) {
                // Assign node to the network manager 
                networkManager.setStartNode(clickedNodeId);
                networkManager.startNode = networkManager.cleanNodeLable((networkManager.nodes.get(clickedNodeId).label));
                networkManager.currentId = clickedNodeId;
                // Move to next task
                tutorial.currentTutorialCount += 1;
                tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
                // Check end node click
              } else if (tutorial.currentTutorialCount == 12) {
                // Only if start and end nodes are different set the end node and route 
                if (networkManager.endNodeDiffersFromStart(clickedNodeId)) {
                  networkManager.endNode = networkManager.cleanNodeLable((networkManager.nodes.get(clickedNodeId).label));
                  networkManager.endNodeId = clickedNodeId;
                  // Move to next task 
                  tutorial.currentTutorialCount += 1;
                  networkManager.routeIsSet = true;
                  // No end is set if equal to start 
                } else {
                  networkManager.endNode = null;
                  tutorial.tutorialTasks[tutorial.currentTutorialCount].text = 'Your journey starts from this station, please select another destination';
                }
                tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
                // Check node click on table construction
              } else if (tutorial.currentTutorialCount == 13) {
                // Check if clicked node is valid for the algorithm steps
                tutorial.tutorialTasks[tutorial.currentTutorialCount].text = tableValidator.dynamicTableConstruction(clickedNodeId);
                tutorial.tutorialTasks[tutorial.currentTutorialCount].set();
              }
            }
          });
          // No longer need to run this scope
          clearInterval(waitForGraphCreation);
        }
      });
    });
  }
}