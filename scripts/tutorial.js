// Class which contains the tutorial text and guide for every task of the simulation
class Tutorial {
  constructor() {
    this.currentTutorialCount = 0;
    this.tutorialTasks = [];
    this.currentGraphSelection = '';
  }

  // Assign text and set current tutorial task to display
  setTutorialLayout() {
    this.tutorialTasks = [{
        text: "Welcome, I will be your Task Manager and Guide <button class = 'button-next'> Next</button>",
        set: function () {
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Select one of the underground networks from the map above",
        set: function () {
          $('#map-container').css('display', 'block');
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Do you need a tour of the main sections of the plugin ? <button class = 'button-next'> Yes </button> <button class = 'button-skip'>Skip</button>",
        set: function () {
          $('#interaction-table-grid').addClass('non-active');
          $('#graph-grid').addClass('non-active');
          $('#suggestion').html(this.text);
          $('.icon-active').css('pointer-events', 'all');
        }
      },
      {
        text: "Here you can find the Dijkstra's table linked to the underground network<button class = 'button-next'>Next</button>",
        set: function () {
          $('#interaction-table-grid-transparent').css('display', 'none');
          $('#interactive-table-container-transparent').css('display', 'none');
          $('#interactive-table-container').addClass('highlightBorder');
          $('#interactive-table-buttons-container-transparent').css('display', 'block');
          $('#graph-grid-transparent').css('display', 'block');
          $('#graph-container-transparent').css('display', 'block');
          $('#graph-legend-container-transparent').css('display', 'block');
          $('#help').removeClass('highlightBorder');
          $('#interaction-table-grid').addClass('non-active');
          $('#graph-grid').addClass('non-active');
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Here you can reset your current network or select a new one from the map<button class = 'button-next'>Next</button>",
        set: function () {
          $('#interaction-table-grid-transparent').css('display', 'none');
          $('#interactive-table-container-transparent').css('display', 'block');
          $('#interactive-table-container').removeClass('highlightBorder');
          $('#interactive-table-buttons-container-transparent').css('display', 'none');
          $('#interactive-table-buttons-container').addClass('highlightBorder');
          $('#graph-grid-transparent').css('display', 'block');
          $('#graph-container-transparent').css('display', 'block');
          $('#graph-legend-container-transparent').css('display', 'block');
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Here you can manipulate the graph and check the network status<button class = 'button-next'>Next</button>",
        set: function () {
          $('#interaction-table-grid-transparent').css('display', 'block');
          $('#interactive-table-container-transparent').css('display', 'block');
          $('#interactive-table-buttons-container-transparent').css('display', 'block');
          $('#interactive-table-buttons-container').removeClass('highlightBorder');
          $('#graph-grid-transparent').css('display', 'none');
          $('#graph-container-transparent').css('display', 'none');
          $('#graph-container').addClass('highlightBorder');
          $('#graph-legend-container-transparent').css('display', 'block');
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Here you can view the graph legend <button class = 'button-next'>Next</button>",
        set: function () {
          $('#interaction-table-grid-transparent').css('display', 'block');
          $('#interactive-table-container-transparent').css('display', 'block');
          $('#interactive-table-buttons-container-transparent').css('display', 'block');
          $('#graph-grid-transparent').css('display', 'none');
          $('#graph-container-transparent').css('display', 'block');
          $('#graph-container').removeClass('highlightBorder');
          $('#graph-legend-container-transparent').css('display', 'none');
          $('#graph-legend-container').addClass('highlightBorder');
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Finally you can use the [Help] button to review this information <button class = 'button-next'>Next</button>",
        set: function () {
          $('#interaction-table-grid-transparent').css('display', 'block');
          $('#interactive-table-container-transparent').css('display', 'block');
          $('#interactive-table-buttons-container-transparent').css('display', 'block');
          $('#graph-grid-transparent').css('display', 'block');
          $('#graph-container-transparent').css('display', 'block');
          $('#graph-legend-container-transparent').css('display', 'block');
          $('#graph-legend-container').removeClass('highlightBorder');
          $('#help').addClass('highlightBorder');
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Great, I think you are ready to go !<button class = 'button-next'>Begin</button>",
        set: function () {
          $('#interaction-table-grid-transparent').css('display', 'none');
          $('#interactive-table-container-transparent').css('display', 'none');
          $('#interactive-table-buttons-container-transparent').css('display', 'none');
          $('#graph-grid-transparent').css('display', 'none');
          $('#graph-container-transparent').css('display', 'none');
          $('#graph-legend-container-transparent').css('display', 'none');
          $('#help').removeClass('highlightBorder');
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Position and organise the Graph so that it is clear to you <button class = 'button-next'>Done</button>",
        set: function () {
          $('#interaction-table-grid').removeClass('non-active');
          $('#graph-grid').removeClass('non-active');
          $('#reset-button').addClass('non-active');
          $('#random-button').addClass('non-active');
          $('#suggestion').html(this.text);
          $('.icon-active').css('pointer-events', 'all');

        }
      },
      {
        text: "Make any final adjustments, once you position the graph you cannot change it <button class = 'button-next'>Done</button>",
        set: function () {
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Choose the station where you want to start your journey from",
        set: function () {
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "Choose the station where you want to end your journey at",
        set: function () {
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "",
        set: function () {
          $('#reset-button').removeClass('non-active');
          $('#random-button').removeClass('non-active');
          $('#solution-container').css('display', 'none');
          $('#suggestion').html(this.text);
        }
      },
      {
        text: "",
        set: function () {
          $('#solution-container').css('display', 'block');
          $('#suggestion').html(this.text);
        }
      }
    ];
  }

  // Setting the first tutorial task
  initTutorial() {
    this.tutorialTasks[this.currentTutorialCount].set();
  }
}