// Manages the status of the algorithm step by step and constructs a table for each node.
// Takes also care of the final submission verification
class tableManager {
  constructor(cst_network, networkManager, solution) {
    // custom network created from vis js 
    this.customNetwork = cst_network;
    // table reference 
    this.table = $('#underground-table')[0];
    // network created from vis js
    this.networkManager = networkManager;
    // current table row
    this.currentRow = 0;
    // are we in the same row 
    this.sameRow = true;
    // remaining neigbours to visit every iteration 
    this.remainNeighbour = 0;
    // check the status of the table
    this.status = 'active';
    // Values for each row
    this.rowValues = [];
    // Possible next nodes
    this.possibleNextNodes = [];
    // only trigger one time
    this.firstTime = true;
    // neighbours 
    this.neighbours = [];
    // solution object with all the nodes
    this.solution = solution;
    // user solution provided
    this.userSolution = [];
  }

  // Reset table properties 
  resetTable() {
    // current table row
    this.currentRow = 0;
    // are we in the same row 
    this.sameRow = true;
    // remaining neigbours to visit every iteration 
    this.remainNeighbour = 0;
    // check the status of the table
    this.status = 'active';
    // Values for each row
    this.rowValues.length = 0;
    // Possible next nodes
    this.possibleNextNodes.length = 0;
    // only trigger one time
    this.firstTime = true;
    // neighbours 
    this.neighbours.length = 0;
    // user solution provided
    this.userSolution.length = 0;
  }

  // Helper function to reset the table cells on current graph 
  resetTableCells() {
    for (let row = 1; row < this.table.rows.length; row++) {
      for (let col = 0; col < this.table.rows.length; col++) {
        this.table.rows[row].cells.item(col).innerHTML = '';
      }
    }

    for (let row = 0; row < this.table.rows.length; row++) {
      for (let col = 0; col < this.table.rows.length; col++) {
        this.table.rows[row].cells.item(col).classList.remove('cell-highlight');
        this.table.rows[row].cells.item(col).classList.remove('finalised-node');
        this.table.rows[row].cells.item(col).classList.remove('finalised');
      }
    }
  }

  // Helper function to reset table cells on new graph
  resetAllTableCells() {
    for (let row = 0; row < this.table.rows.length; row++) {
      for (let col = 0; col < this.table.rows.length; col++) {
        if (!(row === 0 && col === 0))
          this.table.rows[row].cells.item(col).innerHTML = '';
      }
    }
    for (let row = 0; row < this.table.rows.length; row++) {
      for (let col = 0; col < this.table.rows.length; col++) {
        this.table.rows[row].cells.item(col).classList.remove('cell-highlight');
        this.table.rows[row].cells.item(col).classList.remove('finalised-node');
        this.table.rows[row].cells.item(col).classList.remove('finalised');
      }
    }
  }

  initTable() {
    // Setting up the first row of the table (all the stations)
    for (let index = 1; index < this.table.rows.length; index++) {
      console.log(this.customNetwork.nodes);
      this.table.rows[this.currentRow].cells.item(index).innerHTML = (this.customNetwork.nodes[index - 1]).substr(this.customNetwork.nodes[index - 1].indexOf('[', 2));
    }
    // Increment the row number 
    this.currentRow += 1;
    this.prevRow = this.currentRow;
    // Assigning the starting node to the table (starting station)
    this.table.rows[this.currentRow].cells.item(0).innerHTML = this.networkManager.startNode.substr(this.networkManager.startNode.indexOf('[', 2));
    // Set initial node distance to 0 with itself on the table
    let id = this.getIdFromLabel([this.table.rows[this.currentRow].cells.item(0).innerHTML]);
    this.table.rows[this.currentRow].cells.item(id).innerHTML = '0 Km ' + this.table.rows[this.currentRow].cells.item(0).innerHTML.substr(0, 4);
    this.table.rows[this.currentRow].cells.item(id).classList.add('finalised-node');
    // Update the distance label on the initial node
    let labelCopy = this.networkManager.cleanNodeLable(this.networkManager.nodes._data[id].label);
    this.networkManager.nodes.update({
      id: id,
      label: labelCopy + '\n D = (0) Km',
      finalised: true
    });
    // Starting task after table setup
    return 'Visit all the stations directly connected to \'' + this.networkManager.cleanNodeLable(this.networkManager.nodes._data[id].label) + '\' station';
  }

  // Helper function to get nodeId from label
  getIdFromLabel(label) {
    for (let index = 0; index < this.networkManager.nodes.length; index++) {
      if (this.networkManager.nodes._data[index + 1].label.includes(label)) {
        return this.networkManager.nodes._data[index + 1].id;
      }
    }
  }

  // Dynamic table construction based on user click events
  dynamicTableConstruction(clickedNodeId) {
    // Only calculate the neigbours here on the first table iteration
    if (this.firstTime) {
      // Loop the edges data and retrive all the node neighbours from the current clicked node 
      for (let item in this.networkManager.edges._data) {
        // Check a to b
        if (this.networkManager.edges._data[item].from === this.networkManager.currentId) {
          // Only consider the neighbour if not finalised
          if (!(this.networkManager.nodes._data[this.networkManager.edges._data[item].to].finalised === true)) {
            this.neighbours.push(this.networkManager.nodes._data[this.networkManager.edges._data[item].to].label);
          }
        }
        // Check b to a
        if (this.networkManager.edges._data[item].to === this.networkManager.currentId) {
          // Only consider the neighbour if not finalised
          if (!(this.networkManager.nodes._data[this.networkManager.edges._data[item].from].finalised === true)) {
            this.neighbours.push(this.networkManager.nodes._data[this.networkManager.edges._data[item].from].label);
          }
        }
      }
      this.remainNeighbour = this.neighbours.length;
      this.firstTime = false;
    }

    // First check if we are on the first row of the table or not
    if (this.currentRow === 1) {
      // Check the status of the oprations, now we are filling the table row 
      if (this.status === 'active') {
        // Check if we are clicking on a finalised node
        if (this.networkManager.nodes._data[clickedNodeId].finalised) {
          return '\'' + (this.networkManager.nodes._data[clickedNodeId].label).substr(0, (this.networkManager.nodes._data[clickedNodeId].label).indexOf('D')) + '\' station is finalised, no more checks are required at this station';
          // Not clicking on a finalised node
        } else {
          // Check if we are clicking on a neighbour
          if (this.neighbours.includes(this.networkManager.nodes._data[clickedNodeId].label) && this.networkManager.nodes._data[clickedNodeId].visited === false) {
            // update status of visited 
            this.networkManager.nodes._data[clickedNodeId].visited = true;
            // One less neighbour to visit
            this.remainNeighbour--;
            // Write the distance on the table and update node label
            for (let item in this.networkManager.edges._data) {
              if (((this.networkManager.edges._data[item].from === this.networkManager.currentId) && (this.networkManager.edges._data[item].to === clickedNodeId)) || ((this.networkManager.edges._data[item].to === this.networkManager.currentId) && (this.networkManager.edges._data[item].from === clickedNodeId))) {
                this.table.rows[this.currentRow].cells.item(clickedNodeId).innerHTML = this.networkManager.edges._data[item].label + ' ' +
                  this.table.rows[this.currentRow].cells.item(0).innerHTML.substr(0, 4);
                // Update the distance label on the initial node
                let labelCopy = this.networkManager.cleanNodeLable(this.networkManager.nodes._data[clickedNodeId].label);
                this.networkManager.nodes.update({
                  id: clickedNodeId,
                  label: labelCopy + '\n D = ' + '(' + [this.networkManager.edges._data[item].label.slice(0, this.networkManager.edges._data[item].label.length - 3), ')', this.networkManager.edges._data[item].label.slice(this.networkManager.edges._data[item].label.length - 3)].join(''),
                  visited: true
                });
              }
            }
            // Check remaining empty cells if all neighbours visited 
            if (this.remainNeighbour === 0) {
              // Update remaining cells with inifinity symbol
              for (let index = 0; index < this.table.rows.length; index++) {
                if (this.table.rows[this.currentRow].cells.item(index).innerHTML === '') {
                  this.table.rows[this.currentRow].cells.item(index).innerHTML = '\u221E';
                }
              }
              // Calculate which node to click next
              for (let index = 1; index < this.table.rows.length; index++) {
                if (!(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised-node')) && !(this.table.rows[this.currentRow].cells.item(index).innerHTML === '\u221E') && !(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised'))) {
                  let stringNum = this.table.rows[this.currentRow].cells.item(index).innerHTML;
                  let floatNum = parseFloat(stringNum.substr(0, stringNum.indexOf(' ').toFixed(1)));
                  this.rowValues.push([Number(floatNum), index]);
                }
              }
              // find the array of minimum values, (possible next nodes)
              let minValue = this.calculateMinValue(this.rowValues);
              for (let index = 0; index < this.rowValues.length; index++) {
                if (minValue[0] === this.rowValues[index][0]) {
                  this.possibleNextNodes.push(this.rowValues[index][1]);
                }
              }
              // Change the status to passive, we have finished with the current row
              this.status = 'passive';
              // highlight table cells to look at
              for (let index = 1; index < this.table.rows.length; index++) {
                if (!(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised-node')) && !(this.table.rows[this.currentRow].cells.item(index).innerHTML === '\u221E') && !(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised'))) {
                  this.table.rows[this.currentRow].cells.item(index).classList.add('cell-highlight');
                  this.table.rows[0].cells.item(index).classList.add('cell-highlight');
                }
              }
              return 'Now look at the table, find the station with the lowest distance and select it on the graph';
            }
            return 'Great, keep visiting all the stations directly connected to \'' + this.networkManager.cleanNodeLable(this.networkManager.nodes._data[this.networkManager.currentId].label) + '\' station'
            // Not clicking on a neighbour node 
          } else if (this.neighbours.some(res => res.includes((this.networkManager.nodes._data[clickedNodeId].label).substr(0, 8)))) {
            return 'You have already visited \'' + (this.networkManager.nodes._data[clickedNodeId].label).substr(0, (this.networkManager.nodes._data[clickedNodeId].label).indexOf('D')) + '\' station';
            // Not directly connected node 
          } else {
            return '\'' + (this.networkManager.nodes._data[clickedNodeId].label).substr(0, (this.networkManager.nodes._data[clickedNodeId].label).indexOf('D')) + '\' station is not directly connecte to ' + '\'' + this.networkManager.cleanNodeLable(this.networkManager.nodes._data[this.networkManager.currentId].label) + '\' station';
          }
        } // Moving to the next row, only need to select the cheapest option before the move 
      } else if (this.status === 'passive') {
        this.neighbours.length = 0;
        // Assign current node id 
        this.networkManager.currentId = clickedNodeId;
        // Check if clicked node correspond to the cheapest route
        if (this.possibleNextNodes.includes(clickedNodeId)) {
          // remove highlight table cells to look at
          for (let index = 1; index < this.table.rows.length; index++) {
            this.table.rows[this.currentRow].cells.item(index).classList.remove('cell-highlight');
            this.table.rows[0].cells.item(index).classList.remove('cell-highlight');
          }
          // Loop the edges data and retrive all the node neighbours from the current clicked node 
          for (let item in this.networkManager.edges._data) {
            // Check a to b
            if (this.networkManager.edges._data[item].from === this.networkManager.currentId) {
              // Only consider the neighbour if not finalised
              if (!(this.networkManager.nodes._data[this.networkManager.edges._data[item].to].finalised === true)) {
                this.neighbours.push(this.networkManager.nodes._data[this.networkManager.edges._data[item].to].label);
              }
            }
            // Check b to a
            if (this.networkManager.edges._data[item].to === this.networkManager.currentId) {
              // Only consider the neighbour if not finalised
              if (!(this.networkManager.nodes._data[this.networkManager.edges._data[item].from].finalised === true)) {
                this.neighbours.push(this.networkManager.nodes._data[this.networkManager.edges._data[item].from].label);
              }
            }
          }
          this.remainNeighbour = this.neighbours.length;
          // Increment the row number
          this.currentRow += 1;
          // Assigning the current node to the table (starting station)
          this.table.rows[this.currentRow].cells.item(0).innerHTML = (this.networkManager.nodes._data[this.networkManager.currentId].label).substr(this.networkManager.nodes._data[this.networkManager.currentId].label.indexOf('['), 4);
          // Copy the value of the cheapest node to the current row 
          this.table.rows[this.currentRow].cells.item(clickedNodeId).innerHTML = this.table.rows[this.currentRow - 1].cells.item(clickedNodeId).innerHTML;
          // Assign the class finalised to the cell
          this.table.rows[this.currentRow].cells.item(clickedNodeId).classList.add('finalised-node');
          // Update node to visited colour except start and end node 
          if (clickedNodeId === this.networkManager.endNodeId) {
            this.networkManager.nodes.update({
              id: clickedNodeId,
              finalised: true
            });
          } else {
            this.networkManager.nodes.update({
              id: clickedNodeId,
              color: {
                background: '#57a157',
                border: '#ffffff',
                highlight: '#eaaa3a',
                hover: '#eaaa3a'
              },
              finalised: true
            });
          }
          // Reset same row variable
          // check if we have no neighbours when picking new node (fill the table row automatically)
          if (this.neighbours.length === 0) {
            this.rowValues.length = 0;
            this.possibleNextNodes.length = 0;
            // Update remaining cells with inifinity symbol
            for (let index = 0; index < this.table.rows.length; index++) {
              if (this.table.rows[this.currentRow].cells.item(index).innerHTML === '') {
                if (this.table.rows[this.currentRow - 1].cells.item(index).classList.contains('finalised-node') || this.table.rows[this.currentRow - 1].cells.item(index).classList.contains('finalised')) {
                  this.table.rows[this.currentRow].cells.item(index).classList.add('finalised');
                  this.table.rows[this.currentRow].cells.item(index).innerHTML = '';
                } else {
                  this.table.rows[this.currentRow].cells.item(index).innerHTML = this.table.rows[this.currentRow - 1].cells.item(index).innerHTML;
                }
              }
            }
            // Calculate which node to click next
            for (let index = 1; index < this.table.rows.length; index++) {
              if (!(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised-node')) && !(this.table.rows[this.currentRow].cells.item(index).innerHTML === '\u221E') && !(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised'))) {
                let stringNum = this.table.rows[this.currentRow].cells.item(index).innerHTML;
                let floatNum = parseFloat(stringNum.substr(0, stringNum.indexOf(' ').toFixed(1)));
                this.rowValues.push([Number(floatNum), index]);
              }
            }
            // find the array of minimum values, (possible next nodes)
            let minValue = this.calculateMinValue(this.rowValues);
            for (let index = 0; index < this.rowValues.length; index++) {
              if (minValue[0] === this.rowValues[index][0]) {
                this.possibleNextNodes.push(this.rowValues[index][1]);
              }
            }
            // Change the status to passive, we have finished with the current row
            this.status = 'passive';
            // highlight table cells to look at
            for (let index = 1; index < this.table.rows.length; index++) {
              if (!(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised-node')) && !(this.table.rows[this.currentRow].cells.item(index).innerHTML === '\u221E') && !(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised'))) {
                this.table.rows[this.currentRow].cells.item(index).classList.add('cell-highlight');
                this.table.rows[0].cells.item(index).classList.add('cell-highlight');
              }
            }
            return 'Now look at the table, find the station with the lowest distance and select it on the graph';
          }
          // Reset visited nodes
          for (let index = 1; index <= this.networkManager.nodes.length; index++) {
            this.networkManager.nodes._data[index].visited = false;
          }
          // Reset status to active table, filling in details
          this.status = 'active';
          // Reset arrays to calculate possible nextNodeToClick
          this.rowValues.length = 0;
          this.possibleNextNodes.length = 0;
          return 'Great, now visit all the stations directly connected to \'' + this.networkManager.cleanNodeLable(this.networkManager.nodes._data[this.networkManager.currentId].label) + '\' station';
          // Not cheapest node of current node, look at the table !
        } else {
          return '' + '\'' + (this.networkManager.nodes._data[clickedNodeId].label).substr(0, (this.networkManager.nodes._data[clickedNodeId].label).indexOf('D')) + '\' station does not have the lowest distance according to the table';
        }
      }
    }
    // Not on the first row of the table 
    else {
      // Check if we are on a active state of the table
      if (this.status === 'active') {
        // Check if we are clicking on a finalised node
        if (this.networkManager.nodes._data[clickedNodeId].finalised) {
          return '\'' + (this.networkManager.nodes._data[clickedNodeId].label).substr(0, (this.networkManager.nodes._data[clickedNodeId].label).indexOf('D')) + '\' station is finalised, no more checks are required at this station';
          // Not clicking on a finalised node
        } else {
          // Check if we are clicking on a neighbour
          if (this.neighbours.includes(this.networkManager.nodes._data[clickedNodeId].label) && this.networkManager.nodes._data[clickedNodeId].visited === false) {
            // update status of visited 
            this.networkManager.nodes._data[clickedNodeId].visited = true;
            // One less neighbour to visit
            this.remainNeighbour--;
            // Write the distance on the table and update node label
            for (let item in this.networkManager.edges._data) {
              if (((this.networkManager.edges._data[item].from === this.networkManager.currentId) && (this.networkManager.edges._data[item].to === clickedNodeId)) || ((this.networkManager.edges._data[item].to === this.networkManager.currentId) && (this.networkManager.edges._data[item].from === clickedNodeId))) {
                // Check if the combined distance is greater than previous distance 
                if (Number((parseFloat((this.networkManager.nodes._data[this.networkManager.currentId].label).split(/[()]/)[1]).toFixed(1))) + Number((parseFloat(this.networkManager.edges._data[item].label.substr(0, this.networkManager.edges._data[item].label.indexOf(' '))).toFixed(1))) >= Number((parseFloat(this.table.rows[this.currentRow - 1].cells.item(clickedNodeId).innerHTML.substr(0, this.table.rows[this.currentRow - 1].cells.item(clickedNodeId).innerHTML.indexOf(' '))).toFixed(1)))) {
                  // Copy the value of the previous row as the current value is larger 
                  this.table.rows[this.currentRow].cells.item(clickedNodeId).innerHTML = this.table.rows[this.currentRow - 1].cells.item(clickedNodeId).innerHTML;
                  // The combined distance is lower for that node 
                } else {
                  // Compute the sum 
                  let a = Number((parseFloat((this.networkManager.nodes._data[this.networkManager.currentId].label).split(/[()]/)[1]).toFixed(1)));
                  let b = Number((parseFloat(this.networkManager.edges._data[item].label.substr(0, this.networkManager.edges._data[item].label.indexOf(' '))).toFixed(1)));
                  let sum = Number((Number(a) + Number(b)).toFixed(1));
                  // Update the table with the new value 
                  this.table.rows[this.currentRow].cells.item(clickedNodeId).innerHTML = sum + ' Km' +
                    ' ' + this.table.rows[this.currentRow].cells.item(0).innerHTML.substr(0, 4);
                  // Update the distance label on the current node with the sum 
                  let labelCopy = this.networkManager.cleanNodeLable(this.networkManager.nodes._data[clickedNodeId].label);
                  this.networkManager.nodes.update({
                    id: clickedNodeId,
                    label: labelCopy + '\n D = (' + sum + ') Km',
                    visited: true
                  });
                }
              }
            }
            // Check remaining empty cells if all neighbours visited 
            if (this.remainNeighbour === 0) {
              // Update remaining cells with previous row content
              for (let index = 0; index < this.table.rows.length; index++) {
                if (this.table.rows[this.currentRow].cells.item(index).innerHTML === '') {
                  if (this.table.rows[this.currentRow - 1].cells.item(index).classList.contains('finalised-node') || this.table.rows[this.currentRow - 1].cells.item(index).classList.contains('finalised')) {
                    this.table.rows[this.currentRow].cells.item(index).classList.add('finalised');
                    this.table.rows[this.currentRow].cells.item(index).innerHTML = '';
                  } else {
                    this.table.rows[this.currentRow].cells.item(index).innerHTML = this.table.rows[this.currentRow - 1].cells.item(index).innerHTML;
                  }
                }
              }
              // Calculate which node to click next
              for (let index = 1; index < this.table.rows.length; index++) {
                if (!(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised-node')) && !(this.table.rows[this.currentRow].cells.item(index).innerHTML === '\u221E') && !(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised'))) {
                  let stringNum = this.table.rows[this.currentRow].cells.item(index).innerHTML;
                  let floatNum = parseFloat(stringNum.substr(0, stringNum.indexOf(' ').toFixed(1)));
                  this.rowValues.push([Number(floatNum), index]);
                }
              }
              // find the array of minimum values, (possible next nodes)
              let minValue = this.calculateMinValue(this.rowValues);
              for (let index = 0; index < this.rowValues.length; index++) {
                if (minValue[0] === this.rowValues[index][0]) {
                  this.possibleNextNodes.push(this.rowValues[index][1]);
                }
              }
              // highlight table cells to look at
              for (let index = 1; index < this.table.rows.length; index++) {
                if (!(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised-node')) && !(this.table.rows[this.currentRow].cells.item(index).innerHTML === '\u221E') && !(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised'))) {
                  this.table.rows[this.currentRow].cells.item(index).classList.add('cell-highlight');
                  this.table.rows[0].cells.item(index).classList.add('cell-highlight');
                }
              }
              // Change the status to passive, we have finished with the current row
              this.status = 'passive';
              return 'Now look at the table, find the station with the lowest distance and select it on the graph';
            }
            return 'Great, keep visiting all the stations directly connected to \'' + this.networkManager.cleanNodeLable(this.networkManager.nodes._data[this.networkManager.currentId].label) + '\' station'
          }
          // Not clicking on a neigbiur node 
          else if ((this.neighbours.some(res => res.includes((this.networkManager.nodes._data[clickedNodeId].label).substr(0, 8))))) {
            return 'You have already visited \'' + (this.networkManager.nodes._data[clickedNodeId].label).substr(0, (this.networkManager.nodes._data[clickedNodeId].label).indexOf('D')) + '\' station';
          } else {
            return '\'' + (this.networkManager.nodes._data[clickedNodeId].label).substr(0, (this.networkManager.nodes._data[clickedNodeId].label).indexOf('D')) + '\' station is not directly connecte to ' + '\'' + this.networkManager.cleanNodeLable(this.networkManager.nodes._data[this.networkManager.currentId].label) + '\' station';
          }
        }
        // Passive status, need to pick the next node to move to 
      } else if (this.status === 'passive') {
        this.neighbours.length = 0;
        // Assign current node id 
        this.networkManager.currentId = clickedNodeId;
        // Check if clicked node correspond to the cheapest route
        if (this.possibleNextNodes.includes(clickedNodeId)) {
          // remove highlight table cells to look at
          for (let index = 1; index < this.table.rows.length; index++) {
            this.table.rows[this.currentRow].cells.item(index).classList.remove('cell-highlight');
            this.table.rows[0].cells.item(index).classList.remove('cell-highlight');
          }
          // Loop the edges data and retrive all the node neighbours from the current clicked node 
          for (let item in this.networkManager.edges._data) {
            // Check a to b
            if (this.networkManager.edges._data[item].from === this.networkManager.currentId) {
              // Only consider the neighbour if not finalised
              if (!(this.networkManager.nodes._data[this.networkManager.edges._data[item].to].finalised === true)) {
                this.neighbours.push(this.networkManager.nodes._data[this.networkManager.edges._data[item].to].label);
              }
            }
            // Check b to a
            if (this.networkManager.edges._data[item].to === this.networkManager.currentId) {
              // Only consider the neighbour if not finalised
              if (!(this.networkManager.nodes._data[this.networkManager.edges._data[item].from].finalised === true)) {
                this.neighbours.push(this.networkManager.nodes._data[this.networkManager.edges._data[item].from].label);
              }
            }
          }
          this.remainNeighbour = this.neighbours.length;
          // Increment the row number
          this.currentRow += 1;
          // Assigning the current node to the table (starting station)
          this.table.rows[this.currentRow].cells.item(0).innerHTML = (this.networkManager.nodes._data[this.networkManager.currentId].label).substr(this.networkManager.nodes._data[this.networkManager.currentId].label.indexOf('['), 4);
          // Copy the value of the cheapest node to the current row 
          this.table.rows[this.currentRow].cells.item(clickedNodeId).innerHTML = this.table.rows[this.currentRow - 1].cells.item(clickedNodeId).innerHTML;
          // Assign the class finalised to the cell
          this.table.rows[this.currentRow].cells.item(clickedNodeId).classList.add('finalised-node');
          // Update node to visited colour except start and end node 
          if (clickedNodeId === this.networkManager.endNodeId) {
            this.networkManager.nodes.update({
              id: clickedNodeId,
              finalised: true
            });
          } else {
            this.networkManager.nodes.update({
              id: clickedNodeId,
              color: {
                background: '#57a157',
                border: '#ffffff',
                highlight: '#eaaa3a',
                hover: '#eaaa3a'
              },
              finalised: true
            });
          }
          // Reset same row variable
          // check if we have no neighbours when picking new node (fill the table row automatically)
          if (this.neighbours.length === 0) {
            this.rowValues.length = 0;
            this.possibleNextNodes.length = 0;
            // Update remaining cells with inifinity symbol
            for (let index = 0; index < this.table.rows.length; index++) {
              if (this.table.rows[this.currentRow].cells.item(index).innerHTML === '') {
                if (this.table.rows[this.currentRow - 1].cells.item(index).classList.contains('finalised-node') || this.table.rows[this.currentRow - 1].cells.item(index).classList.contains('finalised')) {
                  this.table.rows[this.currentRow].cells.item(index).classList.add('finalised');
                  this.table.rows[this.currentRow].cells.item(index).innerHTML = '';
                } else {
                  this.table.rows[this.currentRow].cells.item(index).innerHTML = this.table.rows[this.currentRow - 1].cells.item(index).innerHTML;
                }
              }
            }
            // Calculate which node to click next
            for (let index = 1; index < this.table.rows.length; index++) {
              if (!(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised-node')) && !(this.table.rows[this.currentRow].cells.item(index).innerHTML === '\u221E') && !(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised'))) {
                let stringNum = this.table.rows[this.currentRow].cells.item(index).innerHTML;
                let floatNum = parseFloat(stringNum.substr(0, stringNum.indexOf(' ').toFixed(1)));
                this.rowValues.push([Number(floatNum), index]);
              }
            }
            // find the array of minimum values, (possible next nodes)
            let minValue = this.calculateMinValue(this.rowValues);
            for (let index = 0; index < this.rowValues.length; index++) {
              if (minValue[0] === this.rowValues[index][0]) {
                this.possibleNextNodes.push(this.rowValues[index][1]);
              }
            }
            // Change the status to passive, we have finished with the current row
            this.status = 'passive';
            // highlight table cells to look at
            for (let index = 1; index < this.table.rows.length; index++) {
              if (!(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised-node')) && !(this.table.rows[this.currentRow].cells.item(index).innerHTML === '\u221E') && !(this.table.rows[this.currentRow].cells.item(index).classList.contains('finalised'))) {
                this.table.rows[this.currentRow].cells.item(index).classList.add('cell-highlight');
                this.table.rows[0].cells.item(index).classList.add('cell-highlight');
              }
            }
            // Check if we are finalised with all the nodes
            if (this.areAllNodesFinalised(this.networkManager.nodes._data)) {
              // loop the solution array object to find the solution id list 
              for (let index = 0; index < this.solution['path'].length; index++) {
                let id = this.solution['path'][index];
                let lable = this.table.rows[0].cells.item(id).innerHTML;
                //loop first column and search for value 
                for (let innerIndex = 0; innerIndex < this.table.rows.length; innerIndex++) {
                  if (lable === this.table.rows[innerIndex].cells.item(0).innerHTML) {
                    this.table.rows[innerIndex].cells.item(0).classList.add('cell-highlight');
                  }
                }
              }
              // Highlight the edges to the shortest path 
              for (let index = this.solution['path'].length - 1; index > 0; index--) {
                let currentId = Number(this.solution['path'][index]);
                let prevId = Number(this.solution['path'][index - 1]);
                // loop the edges ids
                for (let item in this.networkManager.edges._data) {
                  // Find the edges to highlight from ---  to
                  if ((this.networkManager.edges._data[item].from === currentId) && (this.networkManager.edges._data[item].to === prevId)) {
                    this.networkManager.edges.update({
                      from: currentId,
                      to: prevId,
                      label: this.networkManager.edges._data[item].label,
                      color: {
                        color: 'rgb(255,0,0)',
                        highlight: '#848484',
                        hover: '#848484'
                      },
                      width: 4
                    });
                  }
                  // Find the edges to highlight to ---  from
                  if ((this.networkManager.edges._data[item].from === prevId) && (this.networkManager.edges._data[item].to === currentId)) {
                    this.networkManager.edges.update({
                      from: prevId,
                      to: currentId,
                      label: this.networkManager.edges._data[item].label,
                      color: {
                        color: 'rgb(255,0,0)',
                        highlight: '#848484',
                        hover: '#848484'
                      },
                      width: 4
                    });
                  }
                }
              }
              return 'The table is now complete and you can submit your answer <button class = \'button-next\'>Submit</button>';
            }
            return 'Now look at the table, find the station with the lowest distance and select it on the graph';
          }
          // Reset visited nodes
          for (let index = 1; index <= this.networkManager.nodes.length; index++) {
            this.networkManager.nodes._data[index].visited = false;
          }
          // Reset status to active table, filling in details
          this.status = 'active';
          this.rowValues.length = 0;
          this.possibleNextNodes.length = 0;
          return 'Great, now visit all the stations directly connected to \'' + this.networkManager.cleanNodeLable(this.networkManager.nodes._data[this.networkManager.currentId].label) + '\' station';
          // Not cheapest node of current node, look at the table !
        } else {
          return '' + '\'' + (this.networkManager.nodes._data[clickedNodeId].label).substr(0, (this.networkManager.nodes._data[clickedNodeId].label).indexOf('D')) + '\' station does not have the lowest distance according to the table';
        }
      }
    }
  }

  // Helper function to grab the minimum value from an array of arrays
  calculateMinValue(array) {
    let min = [10000, 0];
    for (let index = 0; index < array.length; index++) {
      if (array[index][0] < min[0]) {
        min = array[index];
      }
    }
    return min;
  }

  // Helper function to check if all the nodes are finalised
  areAllNodesFinalised(nodes) {
    for (let node in nodes) {
      if (nodes[node].finalised === false) {
        return false;
      }
    }
    return true;
  }

  // Helper function to check if two arrays are equal
  areArraysEqual(solution, correct) {
    if (solution.length !== correct.length) {
      return false;
    }
    for (let item in solution) {
      if (solution[item] !== correct[item]) {
        return false;
      }
    }
    return true;
  }

  //Helper function to reset nodes properties
  resetNodesProperties() {
    // Visited and Finalised 
    for (let index = 1; index <= this.networkManager.nodes.length; index++) {
      this.networkManager.nodes._data[index].visited = false;
      this.networkManager.nodes._data[index].finalised = false;
    }
    // Store current labels 
    let labelArray = [];
    for (let index = 1; index <= this.networkManager.nodes.length; index++) {
      labelArray.push(this.networkManager.nodes._data[index].label.substr(0, this.networkManager.nodes._data[index].label.indexOf('=')));
    }
    // Update labels to initial state
    for (let index = 1; index <= this.networkManager.nodes.length; index++) {
      this.networkManager.nodes.update({
        id: index,
        label: labelArray[index - 1] + ' = \u221E'
      });
    }
  }
}