var $mainContainer;
var mainAimationFinished = false;
var mainAppStarted = false;


// Fading intro animation
$(document).ready(function () {
  var $introContainer = $('#introContainer');
  $mainContainer = $('#main-container-grid');
  var $beginButton = $('#begin');
  var $introImageActive = $('#introImageActive');
  setTimeout(function () {
    $introContainer.fadeIn(4000);
  }, 1000)
  $beginButton.on('click', function () {
    $introImageActive.fadeIn(3000, function () {
      $introContainer.fadeOut(2000, function () {
        $mainContainer.css('display', 'grid');
        $mainContainer.fadeIn(3000, function () {
          mainAimationFinished = true;
        });
      });
    });
  })
})

// Load main app when the animation finishes
var waitingForEndAnimation = setInterval(function () {
  console.log(mainAimationFinished);
  if (mainAimationFinished) {
    $mainContainer.css('visibility', 'visible');
    $(".modal").modal("show");
    mainAppStarted = true;
    clearInterval(waitingForEndAnimation);
  }
}, 1000);