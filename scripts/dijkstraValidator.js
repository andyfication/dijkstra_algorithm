// Class used to find the dijkstra solution on a specific graph
class DijkstraValidator {
  constructor(network) {
    this.visNetwork = network;
    this.map = null;
    this.mapLength = this.visNetwork.body.data.nodes.length;
    this.mapNodes = this.visNetwork.body.data.nodes._data;
    this.mapEdges = this.visNetwork.body.data.edges._data;
  }

  // Initialise the network map
  constructGraph() {
    this.map = new Graph();
    // construct map nodes using vis js network
    for (let nodeId = 1; nodeId <= this.mapLength; nodeId++) {
      this.map.addNode(this.cleanNodeLable(this.mapNodes[nodeId].label));
    }
    // construct map edges using vis js network
    for (let key in this.mapEdges) {
      let from = this.mapEdges[key].from;
      let to = this.mapEdges[key].to;
      let weight = this.cleanEdgeLable(this.mapEdges[key].label);
      this.map.addEdge(this.cleanNodeLable(this.mapNodes[from].label), this.cleanNodeLable(this.mapNodes[to].label), parseFloat(weight));
    }
  }

  //Helper function to clean each node lable (not including the total cost label)
  cleanNodeLable(label) {
    let stringCopy = label;
    stringCopy = stringCopy.substr(0, stringCopy.indexOf('\n'));
    return stringCopy;
  }

  //Helper function to clean each edge lable (not including the Km label)
  cleanEdgeLable(label) {
    let stringCopy = label;
    stringCopy = stringCopy.substr(0, stringCopy.indexOf('K'));
    return stringCopy;
  }

  // Main algorithm calculation
  calculateShortestPath(startNode, endNode) {
    let distances = {};
    let backtrace = {};
    let pq = new PriorityQueue();
    // Set 0 distance on the start node 
    distances[startNode] = 0;
    // Set infinity distance on all nodes except start 
    this.map.nodes.forEach(node => {
      if (node !== startNode) {
        distances[node] = Infinity;
      }
    });
    // Add first node 
    pq.enqueue([startNode, 0]);
    // Calculate shortes path neigbour
    while (!pq.isEmpty()) {
      let shortestStep = pq.dequeue();
      let currentNode = shortestStep[0];
      // Check shortest distance based on the weight of each neigbhor
      this.map.adjacencyList[currentNode].forEach(neighbor => {
        let distance = distances[currentNode] + neighbor.weight;
        if (distance < distances[neighbor.node]) {
          distances[neighbor.node] = distance;
          backtrace[neighbor.node] = currentNode;
          pq.enqueue([neighbor.node, distance]);
        }
      });
    }
    // store the array of nodes id for the solution path
    let path = null;
    for (let item in this.mapNodes) {
      if ((this.mapNodes[item].label).indexOf(endNode) >= 0) {
        path = [this.mapNodes[item].id];
      }
    }
    let lastStep = endNode;
    // update best path 
    while (lastStep !== startNode) {
      for (let item in this.mapNodes) {
        if ((this.mapNodes[item].label).indexOf(backtrace[lastStep]) >= 0) {
          path.unshift(this.mapNodes[item].id);
        }
      }
      lastStep = backtrace[lastStep];
    }
    // Solution from start to end using ids 
    return {
      path: path,
      distance: distances[endNode].toFixed(1)
    };
  }

  // Assign nodes to the solution list
  assignNodesToList() {
    let nodeList = $('#node-label-list li');
    for (let index = 0; index < this.mapLength; index++) {
      $(nodeList[index]).find('span').text(this.cleanNodeLable((this.mapNodes[index + 1].label)));
    }
  }
}

// Class used to construct the copy graph from the vis.js network
class Graph {
  constructor() {
    this.nodes = [];
    this.adjacencyList = {};
  }

  // Add node to the graph data structure
  addNode(node) {
    this.nodes.push(node);
    this.adjacencyList[node] = [];
  }

  // Add edge and weight to the graph data structure (from to and to from)
  addEdge(node1, node2, weight) {
    this.adjacencyList[node1].push({
      node: node2,
      weight: weight
    });
    this.adjacencyList[node2].push({
      node: node1,
      weight: weight
    });
  }
}

// Class used to give priorities to the next nodes to visit
class PriorityQueue {
  constructor() {
    this.collection = [];
  }

  // Add element on the queue based on lower weight
  enqueue(element) {
    if (this.isEmpty()) {
      this.collection.push(element);
    } else {
      let added = false;
      for (let i = 1; i <= this.collection.length; i++) {
        if (element[1] < this.collection[i - 1][1]) {
          this.collection.splice(i - 1, 0, element);
          added = true;
          break;
        }
      }
      if (!added) {
        this.collection.push(element);
      }
    }
  }

  // remove first element from the queue 
  dequeue() {
    let value = this.collection.shift();
    return value;
  }

  //Return true if queue is empty
  isEmpty() {
    return (this.collection.length === 0);
  }
}