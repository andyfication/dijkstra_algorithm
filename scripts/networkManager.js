// Class that initialise the network and checks its status 
class NetworkManager {
  constructor() {
    this.container = $('#network-canvas')[0];
    this.options = {};
    this.nodes;
    this.edges;
    this.data;
    this.network;
    this.startNode = null;
    this.endNode = null;
    this.endNodeId = 0;
    this.graphCreated = false;
    this.graphPositioned = false;
    this.routeIsSet = false;
    this.networkId = null;
    this.currentId = 1;
    this.networkContainer = new NetworkContainer();
  }

  // New graph function
  newNetwork() {
    this.options = {};
    this.nodes;
    this.edges;
    this.data;
    this.network;
    this.startNode = null;
    this.endNode = null;
    this.endNodeId = 0;
    this.graphCreated = false;
    this.graphPositioned = false;
    this.routeIsSet = false;
    this.networkId = null;
    this.currentId = 1;
    this.networkContainer = new NetworkContainer();
  }

  // Reset function
  reset() {
    this.startNode = null;
    this.endNode = null;
    this.endNodeId = 0;
    this.graphCreated = false;
    this.graphPositioned = false;
    this.routeIsSet = false;
    this.currentId = 1;
  }

  // Initialise graph properties and setup
  initialSetup() {
    this.options = {
      physics: {
        enabled: false
      },
      autoResize: true,
      height: '100%',
      width: '100%',
      interaction: {
        hover: true
      },
      nodes: {
        shape: 'dot',
        size: 18,
        color: {
          background: '#ffffff',
          border: '#ffffff',
          highlight: '#eaaa3a',
          hover: '#eaaa3a'
        }
      },
      edges: {
        smooth: {
          enabled: true,
          type: "continuous",
          roundness: 0.5
        },
        color: {
          color: 'rgb(0, 174, 239)',
          highlight: '#848484',
          hover: '#848484',
          inherit: 'from',
          opacity: 0.5
        },
        chosen: false
      },
      layout: {
        improvedLayout: true,
        hierarchical: {
          enabled: false
        }
      }
    };
  }

  // Assign nodes and edges based on the underground selection
  assignNodesandEdgesToNetwork() {

    switch (this.networkId) {
      // London underground nodes 
      case 'london':
        this.nodes = new vis.DataSet([{
            id: 1,
            label: this.networkContainer.nodeList[0][0],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 2,
            label: this.networkContainer.nodeList[0][1],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 3,
            label: this.networkContainer.nodeList[0][2],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 4,
            label: this.networkContainer.nodeList[0][3],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 5,
            label: this.networkContainer.nodeList[0][4],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 6,
            label: this.networkContainer.nodeList[0][5],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          }
        ]);

        // London underground edges
        this.edges = new vis.DataSet([{
            from: this.networkContainer.edgesList[0][0][0],
            to: this.networkContainer.edgesList[0][0][1],
            label: this.networkContainer.edgesList[0][0][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[0][1][0],
            to: this.networkContainer.edgesList[0][1][1],
            label: this.networkContainer.edgesList[0][1][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[0][2][0],
            to: this.networkContainer.edgesList[0][2][1],
            label: this.networkContainer.edgesList[0][2][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[0][3][0],
            to: this.networkContainer.edgesList[0][3][1],
            label: this.networkContainer.edgesList[0][3][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[0][4][0],
            to: this.networkContainer.edgesList[0][4][1],
            label: this.networkContainer.edgesList[0][4][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[0][5][0],
            to: this.networkContainer.edgesList[0][5][1],
            label: this.networkContainer.edgesList[0][5][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[0][6][0],
            to: this.networkContainer.edgesList[0][6][1],
            label: this.networkContainer.edgesList[0][6][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[0][7][0],
            to: this.networkContainer.edgesList[0][7][1],
            label: this.networkContainer.edgesList[0][7][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          }
        ]);
        break;

        // Paris underground nodes 
      case 'paris':
        this.nodes = new vis.DataSet([{
            id: 1,
            label: this.networkContainer.nodeList[1][0],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 2,
            label: this.networkContainer.nodeList[1][1],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 3,
            label: this.networkContainer.nodeList[1][2],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 4,
            label: this.networkContainer.nodeList[1][3],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 5,
            label: this.networkContainer.nodeList[1][4],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 6,
            label: this.networkContainer.nodeList[1][5],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          }
        ]);

        // Paris underground edges
        this.edges = new vis.DataSet([{
            from: this.networkContainer.edgesList[1][0][0],
            to: this.networkContainer.edgesList[1][0][1],
            label: this.networkContainer.edgesList[1][0][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[1][1][0],
            to: this.networkContainer.edgesList[1][1][1],
            label: this.networkContainer.edgesList[1][1][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[1][2][0],
            to: this.networkContainer.edgesList[1][2][1],
            label: this.networkContainer.edgesList[1][2][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[1][3][0],
            to: this.networkContainer.edgesList[1][3][1],
            label: this.networkContainer.edgesList[1][3][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[1][4][0],
            to: this.networkContainer.edgesList[1][4][1],
            label: this.networkContainer.edgesList[1][4][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[1][5][0],
            to: this.networkContainer.edgesList[1][5][1],
            label: this.networkContainer.edgesList[1][5][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[1][6][0],
            to: this.networkContainer.edgesList[1][6][1],
            label: this.networkContainer.edgesList[1][6][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          }
        ]);
        break;

        // Tokyo underground nodes 
      case 'tokyo':
        this.nodes = new vis.DataSet([{
            id: 1,
            label: this.networkContainer.nodeList[2][0],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 2,
            label: this.networkContainer.nodeList[2][1],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 3,
            label: this.networkContainer.nodeList[2][2],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 4,
            label: this.networkContainer.nodeList[2][3],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 5,
            label: this.networkContainer.nodeList[2][4],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 6,
            label: this.networkContainer.nodeList[2][5],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }

          }
        ]);

        // Tokyo underground edges
        this.edges = new vis.DataSet([{
            from: this.networkContainer.edgesList[2][0][0],
            to: this.networkContainer.edgesList[2][0][1],
            label: this.networkContainer.edgesList[2][0][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[2][1][0],
            to: this.networkContainer.edgesList[2][1][1],
            label: this.networkContainer.edgesList[2][1][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[2][2][0],
            to: this.networkContainer.edgesList[2][2][1],
            label: this.networkContainer.edgesList[2][2][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[2][3][0],
            to: this.networkContainer.edgesList[2][3][1],
            label: this.networkContainer.edgesList[2][3][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[2][4][0],
            to: this.networkContainer.edgesList[2][4][1],
            label: this.networkContainer.edgesList[2][4][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[2][5][0],
            to: this.networkContainer.edgesList[2][5][1],
            label: this.networkContainer.edgesList[2][5][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[2][6][0],
            to: this.networkContainer.edgesList[2][6][1],
            label: this.networkContainer.edgesList[2][6][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          }
        ]);
        break;

        // Delhi underground nodes 
      case 'delhi':
        this.nodes = new vis.DataSet([{
            id: 1,
            label: this.networkContainer.nodeList[3][0],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 2,
            label: this.networkContainer.nodeList[3][1],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 3,
            label: this.networkContainer.nodeList[3][2],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 4,
            label: this.networkContainer.nodeList[3][3],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 5,
            label: this.networkContainer.nodeList[3][4],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 6,
            label: this.networkContainer.nodeList[3][5],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }

          }
        ]);

        // Delhi underground edges
        this.edges = new vis.DataSet([{
            from: this.networkContainer.edgesList[3][0][0],
            to: this.networkContainer.edgesList[3][0][1],
            label: this.networkContainer.edgesList[3][0][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[3][1][0],
            to: this.networkContainer.edgesList[3][1][1],
            label: this.networkContainer.edgesList[3][1][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[3][2][0],
            to: this.networkContainer.edgesList[3][2][1],
            label: this.networkContainer.edgesList[3][2][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[3][3][0],
            to: this.networkContainer.edgesList[3][3][1],
            label: this.networkContainer.edgesList[3][3][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[3][4][0],
            to: this.networkContainer.edgesList[3][4][1],
            label: this.networkContainer.edgesList[3][4][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[3][5][0],
            to: this.networkContainer.edgesList[3][5][1],
            label: this.networkContainer.edgesList[3][5][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          }
        ]);
        break;

        // Washington underground nodes 
      case 'washington':
        this.nodes = new vis.DataSet([{
            id: 1,
            label: this.networkContainer.nodeList[4][0],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 2,
            label: this.networkContainer.nodeList[4][1],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 3,
            label: this.networkContainer.nodeList[4][2],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 4,
            label: this.networkContainer.nodeList[4][3],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 5,
            label: this.networkContainer.nodeList[4][4],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }
          },
          {
            id: 6,
            label: this.networkContainer.nodeList[4][5],
            visited: false,
            finalised: false,
            font: {
              size: 12,
              color: '#f5da42',
              face: 'arial'
            }

          }
        ]);

        // Washington underground edges
        this.edges = new vis.DataSet([{
            from: this.networkContainer.edgesList[4][0][0],
            to: this.networkContainer.edgesList[4][0][1],
            label: this.networkContainer.edgesList[4][0][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[4][1][0],
            to: this.networkContainer.edgesList[4][1][1],
            label: this.networkContainer.edgesList[4][1][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[4][2][0],
            to: this.networkContainer.edgesList[4][2][1],
            label: this.networkContainer.edgesList[4][2][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[4][3][0],
            to: this.networkContainer.edgesList[4][3][1],
            label: this.networkContainer.edgesList[4][3][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[4][4][0],
            to: this.networkContainer.edgesList[4][4][1],
            label: this.networkContainer.edgesList[4][4][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          },
          {
            from: this.networkContainer.edgesList[4][5][0],
            to: this.networkContainer.edgesList[4][5][1],
            label: this.networkContainer.edgesList[4][5][2],
            font: {
              size: 15,
              background: '#ffffff'
            }
          }
        ]);
        break;
    }
  }

  // Initialise network structure
  initNetwork() {
    // provide the data in the vis format
    this.data = {
      nodes: this.nodes,
      edges: this.edges
    };
    // initialization
    this.network = new vis.Network(this.container, this.data, this.options);
  }

  // Set start node colour to light blue 
  setStartNode(id) {
    this.nodes.update({
      id: id,
      color: {
        background: 'rgb(0, 174, 239)'
      }
    });
  }

  // Set end node colour to light red only if the end node is different from start node 
  endNodeDiffersFromStart(id) {
    if (this.cleanNodeLable(this.nodes.get(id).label) !== this.startNode) {
      this.nodes.update({
        id: id,
        color: {
          background: '#e84d2a'
        }
      });
      return true;
    } else
      return false;
  }

  //Helper function to clean each node lable (not including the total cost label on the node)
  cleanNodeLable(label) {
    let stringCopy = label;
    stringCopy = stringCopy.substr(0, stringCopy.indexOf('\n'));
    return stringCopy;
  }

  // Helper function to reset each node and edge colours
  resetColours() {
    for (let index = 1; index <= this.nodes.length; index++) {
      this.nodes.update({
        id: index,
        color: {
          background: '#ffffff',
          border: '#ffffff',
          highlight: '#eaaa3a',
          hover: '#eaaa3a'
        }
      });
    }
  }
}