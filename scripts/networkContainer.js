// Class which contains all underground networks
class NetworkContainer {
  constructor() {
    // List of nodes for each network 
    this.nodeList = [
      ['Oxford Circus [OC]\n D = \u221E', 'Green Park [GP]\n D = \u221E', 'Bond Street [BS]\n D = \u221E', 'Piccadilly Circus [PC]\n D = \u221E', 'Charing Cross [CC]\n D = \u221E', 'Leicester Square [LS]\n D = \u221E'],
      ['Pyramides [PR]\n D = \u221E', 'Musée du Louvre [ML]\n D = \u221E', 'Rivoli [RI]\n D = \u221E', 'Châtelet [CH]\n D = \u221E', 'Madeleine [MA]\n D = \u221E', 'Opéra [OP]\n D = \u221E'],
      ['Mitsukoshimae [MI]\n D = \u221E', 'Nihombashi [NH]\n D = \u221E', 'Ningyocho [NI]\n D = \u221E', 'Kayabacho [KA]\n D = \u221E', 'Monzen [MO]\n D = \u221E', 'Kiyosumi [KI]\n D = \u221E'],
      ['Central Secretariat [CS]\n D = \u221E', 'Rajiv [RA]\n D = \u221E', 'Barakhamba [BA]\n D = \u221E', 'Mandi House [MH]\n D = \u221E', 'Janpath [JA]\n D = \u221E', 'Khan Market [KM]\n D = \u221E'],
      ['Metro Center [MC]\n D = \u221E', 'Federal Triangle [FT]\n D = \u221E', 'Smithsonian [SM]\n D = \u221E', 'Enfant Plaza [EP]\n D = \u221E', 'Archives [AR]\n D = \u221E', 'Gallery Place [GP]\n D = \u221E']
    ];
    // List of edges and distances for each network
    this.edgesList = [
      [
        [1, 2, '1.2 Km'],
        [1, 3, '0.6 Km'],
        [1, 4, '1 Km'],
        [2, 3, '1.4 Km'],
        [2, 4, '0.6 Km'],
        [4, 5, '0.5 Km'],
        [4, 6, '0.5 Km'],
        [5, 6, '0.5 Km']
      ],
      [
        [1, 2, '0.5 Km'],
        [1, 4, '1.3 Km'],
        [1, 5, '1.2 Km'],
        [1, 6, '0.8 Km'],
        [2, 3, '0.6 Km'],
        [3, 4, '0.8 Km'],
        [5, 6, '0.7 Km'],
      ],
      [
        [1, 2, '0.9 Km'],
        [2, 3, '1.4 Km'],
        [2, 4, '1 Km'],
        [3, 4, '1.4 Km'],
        [3, 6, '0.8 Km'],
        [4, 5, '2.1 Km'],
        [5, 6, '1.7 Km'],
      ],
      [
        [1, 2, '2.4 Km'],
        [1, 5, '1.7 Km'],
        [1, 6, '2.0 Km'],
        [2, 3, '0.4 Km'],
        [3, 4, '0.7 Km'],
        [4, 5, '1.2 Km'],
      ],
      [
        [1, 2, '0.3 Km'],
        [1, 6, '0.4 Km'],
        [2, 3, '0.4 Km'],
        [3, 4, '0.5 Km'],
        [4, 5, '1.2 Km'],
        [5, 6, '0.5 Km'],
      ]
    ];
  }
}